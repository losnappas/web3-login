import svelte from 'rollup-plugin-svelte';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import polyfills from 'rollup-plugin-node-polyfills';
import svelteSVG from "rollup-plugin-svelte-svg";
// import pkg from './package.json';
import { terser } from 'rollup-plugin-terser';

// const name = pkg.name
// 	.replace(/^(@\S+\/)?(svelte-)?(\S+)/, '$3')
// 	.replace(/^\w/, m => m.toUpperCase())
// 	.replace(/-\w/g, m => m[1].toUpperCase());

export default [{
	input: 'src/index.js',
	output: [
		{ dir: 'dist', 'format': 'es', entryFileNames: '[name].es.js', chunkFileNames: '[name].es.js' },
		// UMD builds are messed!
		// { file: pkg.main, 'format': 'umd', name },
		process.env.PRODUCTION && { dir: 'dist', 'format': 'es', entryFileNames: '[name].min.es.js', chunkFileNames: '[name].min.es.js', plugins: [terser()] }
	].filter(Boolean),

	// Order of plugins matters :(
	plugins: [
		svelte({
		}),
		commonjs(),
		resolve({
			browser: true,
			preferBuiltins: false
		}),
		polyfills(),
		json(),
		svelteSVG()
	]
}];
