import Component from './Component.svelte';
import { modal, login, __ } from './stores.js';

export { modal };

/**
 * Sets a callback for the message that will be signed with personal_sign.
 *
 * @param function getLoginMessage Callback for login message generation.
 * @param function verifySignature Callback for verifying message.
 * @param Object   i18n            Translated strings object.
 */
export function configure(getLoginMessage, verifySignature, i18n) {
	login.update({
		getLoginMessage,
		verifySignature
	});
	if (i18n) {
		__.set(i18n)
	}
}

window.addEventListener('load', () => {
	const id = 'web3-login-root';
	let el = document.getElementById(id);
	if (!el) {
		el = document.createElement('div');
		el.id = id;
		document.body.appendChild(el);
		new Component({
			target: el
		});
	}
});
window.Web3Login = {
	modal,
	configure
};
