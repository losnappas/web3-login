import { writable } from 'svelte/store';

export const modal = createModal();
export const login = createLoginFunctions();
export const __ = writable({
	"calltoaction": "Choose your login method",
	"nodetect": "Error: cannot detect crypto wallet",
	"permission": "Waiting for your permission",
	"fetching": "Fetching login phrase...",
	"awaiting": "Waiting for your signature",
	"verifying": "Verifying signature...",
	"loggedin": "Logged in",
	"aborted": "Login aborted",
	"heading": "Log In",
	"walletconnectButtonTitle": "Scan a QR code with your wallet, https://walletconnect.org",
	"metamaskButtonTitle": "Browser add-on and mobile app, https://metamask.io",
});

function createModal() {
	const { update, set, subscribe } = writable(false);
	return {
		subscribe,
		open: () => set(true),
		close: () => set(false),
		toggle: () => update(n => !n)
	};
}

// This holds things passed to `Web3Login.configure`:
// The getLoginMessage and verifySignature methods.
function createLoginFunctions() {
	const { update, subscribe } = writable({});
	return {
		subscribe,
		update: obj => update(n => ({
			...n,
			...obj
		}))
	};
}
