import WalletConnect from '@walletconnect/client';
import WalletConnectQRCodeModal from '@walletconnect/qrcode-modal';

export default class WalletConnectProvider extends WalletConnect {
	constructor() {
		const bridge = 'https://bridge.walletconnect.org';
		super({ bridge });
		this._enabled = false;
	}

	async enable() {
		if ( this.connected ) {
			return this._accounts;
		}
		if ( this._enabled ) {
			return this._accounts;
		}
		this._enabled = true;
		// The docs on this are not so great?
		return this.createSession()
			.then( () => {
				return new Promise( ( resolve, reject ) => {
					this.on( 'connect', ( error ) => {
						if ( error ) {
							reject( error );
						} else {
							WalletConnectQRCodeModal.close();
							resolve( this._accounts );
						}
					});
					WalletConnectQRCodeModal.open( this.uri, reject );
				});
			});
	}

	async personalSign( address, message ) {
		return this.signPersonalMessage([ message, address ])
			.then(signedMessage => ([ signedMessage, address ]));
	}
}
