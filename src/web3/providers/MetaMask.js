export default class MetaMaskConnector {
	constructor(ethereum) {
		if ( ! ethereum  ) {
			throw new Error( 'MetaMask is not injected.' );
		}
		this._ethereum = ethereum;
	}

	async enable() {
		return this._ethereum.request({
			method: 'eth_requestAccounts'
		});
	}

	async personalSign(address, message) {
		return this._ethereum.request({
			method: 'personal_sign',
			params: [ message, address ],
			from: address
		}).then(signature => ([ signature, address ]));
	}
}
