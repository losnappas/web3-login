# web3-login

A web3 login component with just `.enable` and `personal_sign`.

## Installation as stand-alone

Copy `dist` folder, import as module.

## Installation from source

Make sure you have [NodeJS](https://nodejs.org/en/) and NPM (node package manager) installed, clone this repository, then run

```
$ cd web3-login
$ npm install
$ npm run build
```

You should now have the files in `dist` folder.

## Installation as component

`$ npm i https://gitlab.com/losnappas/web3-login`.

## Example & Try It

![Login flow example gif](https://ps.w.org/ethpress/assets/screenshot-1.gif?rev=2239163)

https://bitflynn.000webhostapp.com/wp-login.php

## Usage

```javascript
import { modal, configure } from 'web3-login';
// Or replace 'web3-login' with your path to dist/index.min.es.js.

configure(function getLoginMessage(addresses) {
	// return [string: message, string: address]
	return [`Hello ${addresses[0]}. Sign here.`, addresses[0]];
}, function verifySignature([ signedMessage, address, provider ]) {
	const type = provider.isMetaMask ? 'metamask' : 'walletconnect';
	return fetch('https://posting_signedMessage_and_address_somewhere_to_confirm'...)
		// Throwing will make it show up on the dialog.
		.then(res => res.ok ? res.json() : throw new Error(res.statusText))
		.then(isSigOk => {
			if (isSigOk) {
				// This would be where you do whatever you wish to do on successful login.
				// Ex.
				document.location = 'https://example.org/success';
				// or maybe
				modal.close();
				return 'Optional message to show instead of "Logged in"';
			}
			else throw new Error('invalid sig.')
		});
}, { // Translations. See src/stores.js for complete list.
	"calltoaction": "Choose your login method",
	"nodetect": "Error: cannot detect crypto wallet",
	"permission": "Waiting for your permission",
	"fetching": "Fetching login phrase...",
	"awaiting": "Waiting for your signature",
	"verifying": "Verifying signature...",
	"loggedin": "Logged in",
	"aborted": "Login aborted",
	"heading": "Log In",
	"walletconnectButtonTitle": "Scan a QR code with your wallet, https://walletconnect.org",
	"metamaskButtonTitle": "Browser add-on and mobile app, https://metamask.io",
});

mybutton.onclick = () => modal.open();
```

Or if using Svelte

```javascript
import Component from 'web3-login';
import { modal, __ } from 'web3-login/src/stores';
```

...I think?

## Styling

Styling the component using CSS should be done using selectors like `#web3-login-root button` instead of `button.svelte-1234567`, because the latter gets automatically generated, and might change between versions.

## Developing

`npm install`, `npm run start` and open `http://localhost:5000/test`.

## Help Me

Add more providers. The goal is to keep the bundle size as low as possible, although at 500kb it's already too huge!!! Keeping it at reasonable size is a true challenge.

We are now using ES modules, because including 600kb for walletconnect doesn't make sense, and wallets probably don't support browsers old enough not to have module support, anyways.

## Used In

https://gitlab.com/losnappas/ethpress in `public/js/main.js`.
